## Role Variables

* `node_exporter_listen_address`: `REQUIRED` - Define the iface address the exporter will listen on



* `node_exporter_port`: `9100` - the port node exporter will listen on



* `node_exporter_version`: `"0.18.1"` - Node exporter version to install



* `node_exporter_sha256`: `"sha256:b2503fd932f85f4e5baf161268854bf5d22001869b84f00fd2d1f57b51b72424"` - the sha256 hash of the node exporter tarball



* `node_exporter_download_url`: `"https://github.com/prometheus/node_exporter/releases/download/v{{node_exporter_version}}/node_exporter-{{node_exporter_version}}.linux-amd64.tar.gz"` - the download url for node exporter



* `node_exporter_disabled_collectors`: `[]` - a list of collectors to disable



* `node_exporter_apt_collector_enabled`: `true` - Enable the apt update metrics textfile collector


