---
- name: Get systemd version
  shell: systemctl --version | awk '$1 == "systemd" {print $2}'
  changed_when: false
  check_mode: false
  register: node_exporter_systemd_version
  tags:
    - skip_ansible_lint

- name: Add node_exporter user
  user:
    name: node_exporter
    append: true
    shell: /usr/sbin/nologin
    system: true
    createhome: false
    home: /

- name: Download Prometheus node_exporter tarball
  get_url:
    url: "{{ node_exporter_download_url }}"
    checksum: "{{ node_exporter_sha256 }}"
    dest: "/tmp/node_exporter-{{ node_exporter_version }}.linux-amd64.tar.gz"

- name: Unpack the node_exporter tarball
  unarchive:
    src: "/tmp/node_exporter-{{ node_exporter_version }}.linux-amd64.tar.gz"
    remote_src: true
    dest: /tmp/
    creates: "/tmp/node_exporter-{{ node_exporter_version }}.linux-amd64"

- name: Copy the node_exporter binary to /usr/local/bin
  copy:
    src: "/tmp/node_exporter-{{ node_exporter_version }}.linux-amd64/node_exporter"
    remote_src: true
    dest: /usr/local/bin/node_exporter
    mode: 0755
    owner: node_exporter
    group: node_exporter
  notify: restart node_exporter

- name: Generate the node_exporter systemd unit
  template:
    src: node_exporter.service.j2
    dest: /etc/systemd/system/node_exporter.service
    mode: 0755
  notify: restart node_exporter

- name: Install moreutils for sponge util
  apt:
    name: moreutils
    state: present

- name: Create texfile collector dir
  file:
    path: "{{ node_exporter_textfile_dir }}"
    state: directory
    owner: node_exporter
    group: node_exporter
    recurse: true
    mode: 0775
  when: node_exporter_textfile_dir | length > 0

- name: Install apt collector
  copy:
    src: apt.sh
    dest: /usr/local/bin/apt-collector
    owner: root
    group: root
    mode: 0775
  when: node_exporter_apt_collector_enabled | bool

- name: Enable apt collector cron
  cron:
    name: apt collector
    minute: '*/15'
    user: root
    job: >
      su -s /bin/sh -c '/usr/local/bin/apt-collector | sponge
      {{ node_exporter_textfile_dir }}/apt.prom' node_exporter
    cron_file: ansible_apt-collector
  when: node_exporter_apt_collector_enabled | bool

- name: Ensure Node Exporter is started and enabled on boot
  systemd:
    daemon_reload: true
    name: node_exporter
    enabled: true
    state: started
